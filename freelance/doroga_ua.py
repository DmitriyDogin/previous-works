# -*- coding: utf-8 -*-
import xlwt
from selenium import webdriver
import sys

from selenium.common.exceptions import NoSuchWindowException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import NoSuchElementException
import time

tabel=xlwt.Workbook(encoding='utf-8')
list=tabel.add_sheet('Харьков', cell_overwrite_ok=True)
list.write(0,0, 'Наименование')
list.write(0,1, 'Номер телефона')
list.write(0,2, 'Описание')
list.write(0,3, 'Адрес')
list.write(0,4, 'email')
list.write(0,5, 'Ссылка')
stroka=1

if len(sys.argv) == 1:
    print('name of file?')
    sys.exit()
driver=webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
f = open(sys.argv[1], 'r')
er = open('err.txt', 'w')
for line in f:
    print(line)
    driver.get(line)
    err = 'none'
    name=''
    descr=''
    adres=''
    href=''
    email=''
    try:
        err = 'name'
        name = driver.find_element_by_xpath(".//*[@id='aspnetForm']/div[4]/table/tbody/tr/td[1]/table/tbody/tr/td[2]/div[2]/div[2]/div[1]/table[1]/tbody/tr/td[1]").text
        err = 'numer'
        numer = driver.find_element_by_xpath(".//*[@id='aspnetForm']/div[4]/table/tbody/tr/td[1]/table/tbody/tr/td[2]/div[2]/div[2]/div[1]/table[2]/tbody/tr[1]/td[2]/div[3]").text
        err = 'descr'
        descr = driver.find_element_by_xpath(".//*[@id='aspnetForm']/div[4]/table/tbody/tr/td[1]/table/tbody/tr/td[2]/div[2]/div[2]/div[4]/div[1]").text
        err = 'adres'
        adres = driver.find_element_by_xpath(".//*[@id='aspnetForm']/div[4]/table/tbody/tr/td[1]/table/tbody/tr/td[2]/div[2]/div[2]/div[1]/table[2]/tbody/tr[1]/td[2]/div[2]").text
        err = 'href'
        try:
            href = driver.find_element_by_xpath(".//*[@id='aspnetForm']/div[4]/table/tbody/tr/td[1]/table/tbody/tr/td[2]/div[2]/div[2]/div[1]/table[2]/tbody/tr[1]/td[2]/noindex/a").get_attribute('href')
        except NoSuchElementException:
            pass
        err = 'email'
        try:
            email = driver.find_element_by_xpath(".//*[@id='ctl00_ctl00_contentPlaceHolderMain_contentPlaceHolderInner_uccRestaurantDetails_hyperLinkEmail1']").text
        except NoSuchElementException:
            pass
    except (NoSuchElementException, StaleElementReferenceException):
        print(err)
        er.write(err)
        er.write('\n')
        er.write(line)
    list.write(stroka, 0, name)
    list.write(stroka, 1, numer)
    list.write(stroka, 2, descr)
    list.write(stroka, 3, adres)
    list.write(stroka, 4, email)
    list.write(stroka, 5, href)
    stroka+=1
tabel.save("doroga.xls")
