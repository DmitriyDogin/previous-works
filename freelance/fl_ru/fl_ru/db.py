from scrapy.utils.project import get_project_settings
import psycopg2

class DB():
    conn = None

    @staticmethod
    def get_conn():
        if DB.conn is not None:
            return DB.conn
        else:
            settings = get_project_settings()
            conn_string = "host='localhost' dbname='" + settings.get('DATABASE')['NAME'] + "' user='" + \
                          settings.get('DATABASE')['USER'] + "' password='" + settings.get('DATABASE')['PASSWORD'] + "'"
            DB.conn = psycopg2.connect(conn_string)
            return DB.conn
