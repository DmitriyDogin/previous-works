# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from fl_ru.db import DB

class WeblancerItem(scrapy.Item):
    title = scrapy.Field()
    description = scrapy.Field()
    link = scrapy.Field()
    amount = scrapy.Field()
    added_date = scrapy.Field()
    
    def save(self, service_id):
        conn = DB.get_conn()
        cursor = conn.cursor()
        query = "INSERT INTO projectsproject_project (title, description, link, amount, created_date, added_date, service_id) VALUES(%s, %s, %s, %s, NOW(), %s, %s)"
        data = (self['title'].replace("'", "\'"), self['description'].replace("'", "\'"), self['link'], self['amount'], self['added_date'], service_id)
        cursor.execute(query, data)
        conn.commit()
    
    @staticmethod
    def get_current_page(service_name):
        conn = DB.get_conn()
        cursor = conn.cursor()
        cursor.execute("SELECT page FROM projectsproject_projectcounter WHERE title LIKE '" + service_name + "'")
        row = cursor.fetchone()
        return str(row[0])

    @staticmethod
    def update_page(page, service_name):
        conn = DB.get_conn()
        cursor = conn.cursor()
        query = "UPDATE projectsproject_projectcounter SET page = %s WHERE title LIKE %s"
        data = (page, service_name)
        cursor.execute(query, data)
        conn.commit()

    @staticmethod
    def get(title, service_id):
        conn = DB.get_conn()
        cursor = conn.cursor()
        cursor.execute("SELECT id FROM projectsproject_project WHERE title LIKE '" + title + "' AND service_id = " + str(service_id))
        return cursor.fetchone()
