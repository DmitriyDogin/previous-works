# -*- coding:  utf-8 -*-
import sys
import re
import time
import scrapy
import fl_ru.items as it
item = it.FlRuItem()

class QuotesSpider(scrapy.Spider):
    name = "fl_ru"
    id = 2
    
    def start_requests(self):
        reload(sys)
        sys.setdefaultencoding('utf8')
        pattern_url = 'https://www.fl.ru/projects/?kind=1&page='
        page = item.get_current_page(service_name=self.name)
        url = pattern_url + page
        yield scrapy.Request(url=url, callback=self.parse)
        
    def parse(self, response):
        page = int(item.get_current_page(service_name=self.name))
        if not response.css('.b-page__lenta .b-post'):
            item.update_page(page=1, service_name=self.name)
            return None
        else:
            item.update_page(page=page + 1, service_name=self.name)
        #f = open('inf.txt', 'w')
        for proj in response.css('.b-page__lenta .b-post'):
            title = proj.css('.b-post__title a::text').extract_first()
            title = title.replace("'", "\'")
            row = item.get(title=title, service_id=self.id)
            if row is None:
                item['title'] = title
                item['link'] = "https://www.fl.ru" + proj.css('.b-post__title a::attr(href)').extract_first()
                description = proj.extract()
                beg = description.find('b-post__txt')
                end = description.find('</div> <div id="project-reason')
                item['description'] = description[beg+15: end]
                item['description'] = item['description'].replace('&nbsp;', '')
                item['description'] = item['description'].replace('&#150;', '-')
                item['description'] = item['description'].replace('&quot;', '"')
                beg = description.find('b-post__price_float_right">')
                end = description.find("</div>');</script>")
                cost = description[beg+28: end]
                if cost.find(u'По договоренности')!=-1:
                    item['amount'] = '0'
                else:
                    item['amount'] = cost[:cost.find('&nbsp')] 
                beg = description.find('</span>&nbsp;&nbsp; ')
                added_date = description[beg+20: ]
                end1 = added_date.find('&nbsp;&nbsp;')
                print(beg, end1)
                added_date = added_date[: end1]
                if added_date.find(u'Только что')!=-1:
                    year = int(time.strftime('%Y'))
                    month = int(time.strftime('%m'))
                    day = int(time.strftime('%d'))   
                    hour = int(time.strftime('%H'))
                    minute = int(time.strftime('%M'))
                    if month < 10:
                        str_month = '0' + str(month)
                    else:
                        str_month = str(month)
                    if day < 10:
                        str_day = '0' + str(day)
                    else:
                        str_day = str(day)
                    if hour<10:
                        str_hour = '0'+str(hour)
                    else:
                        str_hour = str(hour)
                    if minute < 10:
                        str_minute = '0' + str(minute)
                    else:
                        str_minute = str(minute)
                    item['added_date'] = str(year) + '-' + str_month + '-' + str_day + ' ' + str_hour + ':'+str_minute+':00'
                else:
                    if added_date.find(u'минут')!=-1 or added_date.find(u'час')!=-1:
                        if added_date.find(u'часа')!=-1:
                            timer = added_date.split(u' часа ')
                        elif added_date.find(u' часов ')!=-1:
                            timer = added_date.split(u' часов ')
                        else:
                            timer = added_date.split(u' час ')
                        if len(timer) == 2:
                            hour = int(timer[0])
                            if timer[1].find(u'минут')==-1:
                                minute = 0
                            else:
                                minute = int(timer[1][:timer[1].find(' ')])
                        else:
                            hour = 0
                            minute = int(timer[0][:timer[0].find(' ')])
                        hour = int(time.strftime('%H'))  - hour
                        minute = int(time.strftime('%M')) - minute 
                        if minute<0:
                            minute+=60
                            hour-=1
                        if hour<0:
                            hour = 24 + hour
                        year = int(time.strftime('%Y'))
                        month = int(time.strftime('%m'))
                        day = int(time.strftime('%d'))
                        if int(time.strftime('%H'))<hour:
                            day-=1
                            if day == 0:
                                if month == 1:
                                    year-=1
                                    month = 12
                                    day = 31
                                else:
                                    month -= 1
                                    if month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12:
                                        day = 31
                                    elif month == 2:
                                        day = 28
                                    else:
                                        day = 30 
                        if month < 10:
                            str_month = '0' + str(month)
                        else:
                            str_month = str(month)
                        if day < 10:
                            str_day = '0' + str(day)
                        else:
                            str_day = str(day)
                        if hour<10:
                            str_hour = '0'+str(hour)
                        else:
                            str_hour = str(hour)
                        if minute < 10:
                            str_minute = '0' + str(minute)
                        else:
                            str_minute = str(minute)
                        item['added_date'] = str(year) + '-' + str_month + '-' + str_day + ' ' + str_hour+ ':'+str_minute+':00'
                    else:
                        date = added_date.split(' ')
                        if date[1] == u'января,':
                            month = 1
                        elif date[1] == u'февраля,':
                            month = 2
                        elif date[1] == u'марта,':
                            month = 3
                        elif date[1] == u'апреля,':
                            month = 4
                        elif date[1] == u'мая,':
                            month = 5
                        elif date[1] == u'июня,':
                            month = 6
                        elif date[1] == u'июля,':
                            month = 7
                        elif date[1] == u'августа,':
                            month = 8
                        elif date[1] == u'сентября,':
                            month = 9
                        elif date[1] == u'октября,':
                            month = 10
                        elif date[1] == u'ноября,':
                            month = 11
                        elif date[1] == u'декабря,':
                            month = 12
                        day = int(date[0])
                        year = int(time.strftime('%Y'))
                        cur_month = int(time.strftime('%m'))
                        if month > cur_month:
                            year-=1
                        item['added_date'] =  str(year) + '-' + str(month) + '-' + str(day) + ' ' + date[2]+':00'
                item.save(service_id=self.id)
            '''f.write(title)
            f.write('\n')
            f.write(item['description'])
            f.write('\n')
            f.write(item['amount'])
            f.write('\n')
            f.write(item['link'])
            f.write('\n')
            f.write(item['added_date'])
            f.write('\n')
            f.write('\n')'''
