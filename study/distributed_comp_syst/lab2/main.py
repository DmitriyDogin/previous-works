import sys
from PyQt5.QtWidgets import QApplication, QCheckBox, QDateTimeEdit, QGridLayout, QInputDialog, QLabel, QLineEdit, QHBoxLayout, QMainWindow, QPushButton, QFrame, QSizePolicy, QSplitter, QWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt, QDate, QSize, QTimer

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from datetime import datetime, timedelta
import numpy as np
import pyopencl as cl
import random
import requests
import sqlite3

class DeleteDialog (QInputDialog):
    def __init__ (self):
        super.__init__ ()
        self.setOkButtonText ("delete")

class DashBthTrader (QMainWindow):

    blockSize = 80
    statusBarHeight = 45

    def __init__ (self):
        super().__init__()

        self.initUI()


    def initUI (self):
        self.connectDB()
        self.loadPhones()

        leftUpFrame = QFrame()
        leftUpFrame.setFrameStyle (QFrame.Panel | QFrame.Raised)
        leftUpFrame.setLineWidth (4)

        cbShowBth = QCheckBox ("BTH graphick", leftUpFrame)
        cbShowBth.setChecked (True)
        cbShowBth.move (self.blockSize / 2, self.blockSize / 2)
        cbShowBth.stateChanged.connect (self.rePlotbyCheckBox)

        cbShowDash = QCheckBox ("DASH graphick", leftUpFrame)
        cbShowDash.setChecked (True)
        cbShowDash.move (self.blockSize / 2, self.blockSize)
        cbShowDash.stateChanged.connect (self.rePlotbyCheckBox)

        lblBegDate = QLabel ("Begine date", leftUpFrame)
        lblBegDate.move (self.blockSize/2, 2 * self.blockSize)

        begDate = QDateTimeEdit (leftUpFrame)
        begDate.setCalendarPopup (True)
        begDate.setDisplayFormat("yyyy.MM.dd")
        begDate.move (self.blockSize / 2, 2.5 * self.blockSize)
        begDate.dateChanged.connect (self.rePlotbyBegDate)

        lblEndDate = QLabel ("End date", leftUpFrame)
        lblEndDate.move (self.blockSize/2, 3 * self.blockSize)

        endDate = QDateTimeEdit (leftUpFrame)
        endDate.setCalendarPopup (True)
        endDate.setDisplayFormat("yyyy.MM.dd")
        endDate.move (self.blockSize / 2, 3.5 * self.blockSize)
        endDate.dateChanged.connect (self.rePlotbyEndDate)

        leftDownFrame = QFrame()
        leftDownFrame.setFrameStyle (QFrame.Panel | QFrame.Raised)
        leftDownFrame.setLineWidth (4)

        lblAddPhone = QLabel ("Phone number", leftDownFrame)
        self.qleAddPhone = QLineEdit (leftDownFrame)
        btnAddPhone = QPushButton ("Add phone", leftDownFrame)
        lblAddPhone.move (self.blockSize / 2, self.blockSize / 2)
        self.qleAddPhone.move (self.blockSize / 2, self.blockSize)
        btnAddPhone.move (self.blockSize / 2, self.blockSize * 1.5)
        self.qleAddPhone.setInputMask("+7 999 999 99 99;_")
        self.qleAddPhone.returnPressed.connect (self.addPhone)
        btnAddPhone.clicked.connect (self.addPhone)

        btnDeletePhone = QPushButton ("Delete phone", leftDownFrame)
        btnDeletePhone.move (self.blockSize / 2, self.blockSize * 3.5)
        btnDeletePhone.clicked.connect (self.showDeleteDialog)

        splitterV = QSplitter (Qt.Vertical)
        splitterV.addWidget (leftUpFrame)
        splitterV.addWidget (leftDownFrame)

        rightFrame = QFrame()
        rightFrame.setFixedSize (self.blockSize*12, self.blockSize*9 - self.statusBarHeight)
        rightFrame.setFrameStyle (QFrame.Panel | QFrame.Raised)
        rightFrame.setLineWidth (4)

        timer = QTimer (self)
        timer.timeout.connect (self.refreshData)
        timer.start (300000)
        timer.singleShot (5000, self.refreshData)

        self.canvas = PlotCanvas (rightFrame)
        begDateRange, endDateRange = self.canvas.getDateRange()
        begDate.setDateRange (begDateRange, endDateRange)
        begDate.setDate (begDateRange)
        endDate.setDateRange (begDateRange, endDateRange)
        endDate.setDate (endDateRange)

        splitterH = QSplitter (Qt.Horizontal)
        splitterH.addWidget (splitterV)
        splitterH.addWidget (rightFrame)

        hbl = QHBoxLayout()
        hbl.addWidget (splitterH)

        centralWidget = QWidget()
        centralWidget.setLayout (hbl)

        '''===========main widget============'''
        self.setCentralWidget (centralWidget)

        self.statusBar()

        self.setFixedSize (self.blockSize*16, self.blockSize*9)
        self.setWindowTitle ('DashBthTrader')

        self.show()

    def connectDB (self):
        self.conn = sqlite3.connect ("traderdb.db")
        self.cursor = self.conn.cursor()

    def loadPhones (self):
        self.phones = []
        self.cursor.execute ("SELECT * FROM phones")
        for phone in self.cursor.fetchall():
            self.phones.append (phone[0])

    def savePhones (self):
        self.cursor.execute ("DELETE FROM phones")
        for phone in self.phones:
            self.cursor.execute ("INSERT INTO phones VALUES (?)", (phone,))
        self.conn.commit()

    def addPhone (self):
        phone = self.qleAddPhone.text()
        if len (phone) == 16:
            self.statusBar().showMessage ("Add phone " + phone)
            self.phones.append (phone)
            self.savePhones()
            self.qleAddPhone.setText ("")
        else:
            self.statusBar().showMessage ("Incorrect phone number")

    def refreshData (self):
        self.statusBar().showMessage ("Load data from API...")
        self.canvas.getDataFromAPI()
        self.canvas.checkRate (self.phones)
        self.canvas.plot()
        self.statusBar().showMessage ("")

    def rePlotbyCheckBox (self, state):
        if self.sender().text() == "BTH graphick":
            self.canvas.showBth = state == Qt.Checked
        else:
            self.canvas.showDash = state == Qt.Checked
        self.canvas.plot()

    def rePlotbyBegDate (self, date):
        self.canvas.begDate = datetime (date.year(), date.month(), date.day(), 0, 0, 0)
        self.canvas.plot()
    
    def rePlotbyEndDate (self, date):
        self.canvas.endDate = datetime (date.year(), date.month(), date.day(), 23, 59, 59)
        self.canvas.plot()

    def showDeleteDialog (self):
        text, ok = QInputDialog.getItem (self, "Delete Phone", "Select phone you want to delete", self.phones)
        if ok:
            self.phones.remove (text)
            self.savePhones()
        


class PlotCanvas (FigureCanvas):
    showBth = True
    showDash = True
    datetimeMask = "%Y-%m-%d %H:%M:%S"
    bthTableName = "bthRate"
    dashTableName = "dashRate"

    def __init__ (self, parent=None, width=12, height=9, dpi=80):
        fig = Figure (figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot (111)

        FigureCanvas.__init__(self, fig)
        self.setParent (parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.ax = self.figure.add_subplot(111)
        self.connectDB()
        self.loadData()

        self.plot()
 
    def plot(self):
        self.ax.clear()
        if self.showBth:
            datas = self.interpolate (self.bthDates, self.bthPrices)
            (xlBth, ylBth, xr, yr) = datas
            self.ax.plot (xlBth, ylBth, label="BTH", color="green")
            self.ax.scatter (xr, yr, color="blue")
        if self.showDash:
            datasd = self.interpolate (self.dashDates, self.dashPrices)
            (xlDash, ylDash, xd, yd) = datasd
            self.ax.plot (xlDash, ylDash,  label="DASH", color="red")
            self.ax.scatter (xd, yd, color="yellow")
        self.ax.legend (loc="best")
        self.ax.set_title('Bithereum and Dashcoin rates')
        self.draw()

    def connectDB (self):
        self.conn = sqlite3.connect ("traderdb.db")
        self.cursor = self.conn.cursor()

    def createDataBase (self):
        self.connectDB ()

        self.cursor.execute ("""CREATE TABLE dashRate (date TEXT PRIMARY KEY, price REAL)""")
        self.cursor.execute ("""CREATE TABLE bthRate (date TEXT PRIMARY KEY, price REAL)""")
        self.cursor.execute ("""CREATE TABLE phones (phone TEXT PRIMARY KEY)""")

    def getDateRange (self):
        return (self.begDate, self.endDate)

    def checkRate (self, phones):
        bthRate = self.bthPrices[len (self.bthPrices) - 1]
        dashRate = self.dashPrices[len (self.dashPrices) - 1]
        bthChange = bthRate != self.bthPrices[len (self.bthPrices) - 2]
        dashChange = dashRate != self.dashPrices[len (self.dashPrices) - 2]
        if len (phones) == 0:
            return
        to = phones[0]
        if len (phones) > 1:
            for phone in phones:
                to += ", " + phone
        if bthRate > 250 and dashRate > 109 and bthChange and dashChange:
            msg = "BTH and DASH rates increased"
        elif dashRate > 109 and dashChange:
            msg = "DASH rate increased"
        elif bthRate > 250 and bthChange:
            msg = "BTH rate increased"
        else:
            return
        "https://sms.ru/sms/send?api_id=38848B9B-13A2-8290-646F-D3A89F53A0BF&to=79991219326,74993221627&msg=hello+world&json=1"
        data = {'api_id':'38848B9B-13A2-8290-646F-D3A89F53A0BF', 'to':to, 'msg':msg, 'json':'1'}
        req = requests.post ("https://sms.ru/sms/send", data=data)


    def getDataFromAPI (self):
        url = 'https://bankersalgo.com/apirates2/5cb323ed68b70/e72b241b7a88f8234d34527e51fda8c0/EUR'
        resp = requests.get (url)
        if resp.status_code != 200:
            return
        if resp.json()['success'] != 'true':
            return
        date = datetime.strptime (resp.json()['date'], self.datetimeMask) + timedelta (hours=1)
        bth = 1 / resp.json()['rates']['CRYPTO_BCH']
        dash = 1 / resp.json()['rates']['CRYPTO_DASH']

        dates = [date]
        bthPrices = [bth]
        dashPrices = [dash]

        self.combineData (bthDates=dates, bthPrices=bthPrices, dashDates=dates, dashPrices=dashPrices)
        self.saveData()

    def _doCombineData (self, oldDates, oldPrices, dates, prices):
        for i in range (len (dates)):
            if len (oldDates) == 0:
                oldDates = dates.copy()
                oldPrices = prices.copy()
                
            elif dates[i] > oldDates[len (oldDates) - 1]:
                oldDates.append (dates[i])
                oldPrices.append (prices[i])
            else:
                for j in range (i, len (oldDates)):
                    if dates[i] == oldDates[j]:
                        break
                    elif dates[i] < oldDates[j]:
                        oldDates = oldDates[:j] + [dates[i]] + oldDates[j:]
                        oldPrices = oldPrices[:j] + [prices[i]] + oldPrices[j:]
                        break

    def combineData (self, bthDates=[], bthPrices=[], dashDates=[], dashPrices=[]):
        self._doCombineData (self.bthDates, self.bthPrices, bthDates, bthPrices)
        self._doCombineData (self.dashDates, self.dashPrices, dashDates, dashPrices)

    def _doLoadData (self, dates, prices, tableName):
        query = "SELECT * FROM " + tableName
        self.cursor.execute (query)
        for row in self.cursor.fetchall():
            date = datetime.strptime (row[0], self.datetimeMask)
            if len(prices) == 0 or prices [len (prices)-1] != row[1]:
                dates.append (date)
                prices.append (row[1])

    def loadData (self):
        self.bthDates = []
        self.bthPrices = []
        self._doLoadData (self.bthDates, self.bthPrices, self.bthTableName)
        self.dashDates = []
        self.dashPrices = []
        self._doLoadData (self.dashDates, self.dashPrices, self.dashTableName)

        self.begDate = QDate (self.bthDates[0].year, self.bthDates[0].month, self.bthDates[0].day + 1)
        dateSize = len (self.bthDates)
        self.endDate = QDate (self.bthDates[dateSize-1].year, self.bthDates[dateSize-1].month, self.bthDates[dateSize-1].day)

    def _doSaveData (self, dates, prices, tablename):
        query = "INSERT OR IGNORE INTO " + tablename + " VALUES (?,?)"
        for i in range (len (dates)):
            self.cursor.execute (query, (dates[i], prices[i]))
        self.conn.commit()

    def saveData (self):
        self._doSaveData (self.bthDates, self.bthPrices, self.bthTableName)
        self._doSaveData (self.dashDates, self.dashPrices, self.dashTableName)

    def interpolate (self, dates, prices):
        xCut = []
        yCut = []
        begin = dates[0]
        for i in range (len (dates)):
            if dates[i] > self.endDate:
                break
            if dates[i] > self.begDate:
                xCut.append ((dates[i]-begin).total_seconds())
                yCut.append (prices[i])
        
        begin += timedelta (seconds=xCut[0])

        xSel = [xCut[len(xCut)-1]]
        ySel = [yCut[len(yCut)-1]]
        step = (xCut[len(xCut)-1] - xCut[0]) / 15
        for i in range (len (xCut)-1, -1, -1):
            if xSel[len(xSel)-1] - xCut[i] > step:
                xSel.append (xCut[i])
                ySel.append (yCut[i])

        xSel.reverse()
        ySel.reverse()

        xSel = [x - xSel [0] for x in xSel]

        xArr = np.asarray (xSel).astype (np.float32)
        yArr = np.asarray (ySel).astype (np.float32)

        xRes = np.arange (xArr [0], xArr [len(xArr) - 1], 30)
        yRes = np.zeros (len (xRes), np.float32)

        platform = cl.get_platforms()[0]
        device = platform.get_devices()[0]
        ctx = cl.Context([device])
        queue = cl.CommandQueue (ctx)

        mf = cl.mem_flags
        a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=xArr)
        b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=yArr)
        yRes_g = cl.Buffer(ctx, mf.WRITE_ONLY, yRes.nbytes)
        prg = cl.Program (ctx, open ('algorithm.cl').read()).build()
        prg.lagrange(queue, yRes.shape, None, np.int32( len (xArr)), a_g, b_g, yRes_g)
        cl.enqueue_copy(queue, yRes, yRes_g)

        resDates = []
        for x in xRes:
            resDates.append (begin + timedelta (seconds=x))

        resSelDates = []
        for nd in xSel:
            resSelDates.append (begin + timedelta (seconds=nd))

        return (resDates, yRes,  resSelDates, ySel)


if __name__ == '__main__':

    app = QApplication (sys.argv)
    oit = DashBthTrader()
    sys.exit(app.exec_())
    