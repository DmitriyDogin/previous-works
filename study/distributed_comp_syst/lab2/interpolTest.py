from __future__ import absolute_import, print_function
import numpy as np
import pyopencl as cl

import matplotlib.pyplot as plt

def interpolate (xArr, yArr):
    xRes = np.arange (0, xArr [len(xArr) - 1], 1)
    yRes = np.zeros (len (xRes), np.float32)

    platform = cl.get_platforms()[0]
    device = platform.get_devices()[0]
    ctx = cl.Context([device])
    queue = cl.CommandQueue (ctx)

    mf = cl.mem_flags
    a_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=xArr)
    b_g = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=yArr)
    yRes_g = cl.Buffer(ctx, mf.WRITE_ONLY, yRes.nbytes)
    print (yRes)

    prg = cl.Program (ctx, open ('algorithm.cl').read()).build()

    prg.lagrange(queue, yRes.shape, None, np.int32( len (xArr)), a_g, b_g, yRes_g)

    cl.enqueue_copy(queue, yRes, yRes_g)

    # Check on CPU with Numpy:
    print (yRes)

    return 

'''plt.plot (xArr, yArr)
plt.plot (xRes, yRes)
plt.show()'''