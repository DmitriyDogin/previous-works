__kernel void lagrange (const unsigned int size, __global const float *xl, __global const float *yl, __global float *yres) {
    int gid = get_global_id(0);
    float sum = 0;
    for (int i=0; i<size; ++i) {
        double mul = 1;
        for (int j=0; j<size; ++j) {
            if (i != j) {
                mul *= (30*gid - xl[j])/(xl[i] - xl[j]);
            }
        }
        sum += yl[i] * mul;
    }
    yres[gid] = sum;
}