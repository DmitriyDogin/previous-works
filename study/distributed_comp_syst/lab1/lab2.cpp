#include <algorithm>
#include <assert.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <unistd.h>
#include <signal.h>
using namespace std;

struct ProcInteractInfo
{

	ProcInteractInfo (int _fdWrite[2], int _fdRead[2], bool _isFree);
	ProcInteractInfo ();

	int fdWrite[2];
	int fdRead[2];
	bool isFree;
};

ProcInteractInfo::ProcInteractInfo (int _fdWrite[2], int _fdRead[2], bool _isFree)
{
	for (int i=0; i<2; ++i) {
		fdRead[i] = _fdRead[i];
		fdWrite[i] = _fdWrite[i];
	}
	isFree = _isFree;
}

ProcInteractInfo::ProcInteractInfo()
{
	for (int i=0; i<2; ++i) {
		fdRead[i] = -1;
		fdWrite[i] = -1;
	}
	isFree = true;
}

void parceArgs (int argc, char** argv, int& proc, string& test, string& result)
{
    int i;
    while ((i = getopt(argc, argv, "p:i:o:")) != -1) {
        switch(char(i)) {
            case 'p':
                proc = atoi (optarg);
                break;
            case 'i':
                test = optarg;
                break;
            case 'o':
                result = optarg;
                break;
        }
    }
}

void parallelReverse (const string& test, const string& result, int proc)
{
	ifstream inputFile (test);
	string inLine;
	inputFile >> inLine;
	int len = inLine.size();
	inputFile.close();
	cout << "string's length = " << len << endl;

    map <pid_t, ProcInteractInfo> procMap;
    vector <ProcInteractInfo> fdVec;
    pid_t pid;
    int fdWrite[2];
    int fdRead[2];

    for (int i=0; i<proc; ++i) {
    	cout << "proc\n";
    	pipe (fdWrite);
        pipe (fdRead);
        assert ((fdWrite && fdRead));
        fdVec.push_back (ProcInteractInfo (fdWrite, fdRead, true));
        pid = fork();
        assert (pid >= 0);
        if (pid > 0) { // parent proc
            //cout << "insert " << pid << ' ' << fdWrite[0] << ' ' << fdWrite[1] << '\n';
            procMap.insert (pair <pid_t, ProcInteractInfo> (pid, ProcInteractInfo (fdWrite, fdRead, true)));
            continue;
        } else { // child proc
            //fdVec.pop_back();
            //cout << "cp\n";
            break;
        }
    }

    /*for_each (procMap.begin(), procMap.end(), [] (pair <pid_t, ProcInteractInfo> p) {
    	cout << "p " << p.first << "  " << p.second.fdWrite[0] << ' ' << p.second.fdWrite[1] << " _ " << p.second.fdRead[0] << ' ' << p.second.fdRead[1]  << '\n'; 
    });

    exit (0);*/

    if (pid > 0) {
    	cout << "parent\n";
        inputFile = ifstream (test);
        //ofstream outputFile (result);
        string inLine, outLine;
        //auto mapIterator = procMap.begin();
        char* theChar = new char;
        close (fdWrite[0]);
        while (true) {
        	inputFile >> inLine;
            if (!inputFile) {
                cout << "file is end\n";
            	close (fdWrite[1]);
                break;
            }
            //cout << inLine << '\n';
            //while (true) {
            	//cout << "ccccc\n";
            	//if (mapIterator == procMap.end()) {
            	//	mapIterator = procMap.begin();
            	//}
            	/*close (mapIterator->second.fdRead[0]);
            	outLine = "";
            	while (read (mapIterator->second.fdRead[1], theChar, 1) == 1) {
            		outLine += theChar;
            		cout << "in cicle\n";
            	}
            	close (mapIterator->second.fdRead[1]);*/
            	//if (outLine != "" || mapIterator->second.isFree) {
            		//outputFile << outLine << endl;
            		//cout << "is free\n";
        	//close (fdWrite[0]);
        	write (fdWrite[1], inLine.c_str(), len);
        	//close (fdWrite[1]);
            		//mapIterator->second.isFree = false;
            		//cout << "write to " << mapIterator->second.fdWrite[1] << '\n';
            		//exit(0);
            		//break;
            	//} else {
            		//mapIterator++;
            		//cout << "is next\n";
            	//}
            }
        //}
        cout << "work was completed\n";
        for_each(procMap.begin(), procMap.end(), [] (pair <pid_t, ProcInteractInfo> procInfo) {
        kill (procInfo.first, SIGKILL);
        });
    } else {
    	cout << "child\n";
        for_each (procMap.begin(), procMap.end(), [] (pair <pid_t, ProcInteractInfo> p) {
        cout << "p " << p.first << "  " << p.second.fdWrite[0] << ' ' << p.second.fdWrite[1] << " _ " << p.second.fdRead[0] << ' ' << p.second.fdRead[1]  << '\n'; 
    });
    	char* childChar = new char[len];
    	string childLine;
        auto myPid = getpid();
        ProcInteractInfo pi = procMap[myPid];
        cout << "pi = " << pi.fdWrite[0] << '\n';
    	//close (fdWrite[1]);
        while (true) {
        	//auto myPid = getpid();
        	//cout << "ma pid = " << myPid << "\n";
        	//ProcInteractInfo procInfo = procMap[myPid];
        	//auto anIt = procMap.begin();
        	//while (anIt != procMap.end()) {
        		//procInfo = anIt->second;
        		//cout << procInfo.fdWrite[0] << ' ' << procInfo.fdWrite[1] << '\n';
        		//anIt++;
        	//}
        	//close(fdWrite[1]);
        	//if (read (fdWrite[0], childChar, len) == 1 ) {
        		//childLine += childChar;
        		//cout << "i read " << childChar << "\n";
        	//}
        	//cout << "close prewerwqerqw\n";
        	//close (fdWrite[0]);
        	//break;
        	//close (fdWrite[0]);
        	if (childLine == "") {
        		//cout << "empty string\n";
        		//reverse (childLine.begin(), childLine.end());
        		//close (procInfo.fdRead[0]);
        		//write (procInfo.fdRead[1], childLine.c_str(), childLine.size());
        		//close (procInfo.fdRead[1]);
        	}
        	//cout << childLine << " - childLine\n"; 
        	//cout << "read from " << fdWrite[0] << '\n';
        	//break;
        }
        cout << "end pi " << pi.fdWrite[0] << '\n';
    }
}

int main(int argc, char** argv)
{
    int proc;
    string result, test;
    parceArgs (argc, argv, proc, test, result);

    parallelReverse (test, result, proc);
}