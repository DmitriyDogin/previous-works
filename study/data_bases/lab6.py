import redis
import psycopg2

r = redis.StrictRedis(host='localhost', port=6379, db=1)

connect = psycopg2.connect(database='journal', user='matros', host='localhost', password='2312670')
cursor = connect.cursor()

if __name__ == "__main__":
    while True:
        print("1. Ввести запрос\n2. Очистить кэш\n")
        c = int(input())
        if c == 1:
            zapr = input()
            res = r.get(zapr)
            if res is None:
                print('new query')
                cursor.execute(zapr)
                res = ''
                for row in cursor:
                    res += str(row) + '\n'
                r.set(zapr, res)
            else:
                res = res.decode('utf-8')
                print('query from cash')
            print(res)
        elif c == 2:
            r.flushdb()
        else:
            exit()