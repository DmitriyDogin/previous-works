from bs4 import BeautifulSoup
import requests

from datetime import datetime

from pymongo import MongoClient

import matplotlib.pyplot as plt

client = MongoClient()
db = client.test_database

'''pepa = {"name":"kaska", "mission":"ded"}
posa = db.posa
ide = posa.insert(pepa)
print(ide)
print(db.collection_names())'''

session = requests.Session()

def getPage(url):  
    headers = {"User-Agent":"Mozi.lla/5.0 (Macintosh; Intel Mac OS X 10_9_5)AppleWebKi.t 537.36 (KHTML, like Gecko) Chro111e","Accept":"text/htril,appli.cati.on/xht111l+x111l,appli.cati.on/x111l;q=0.9,i.111age/webp,*/*;q=0.8"}
    req=session.get(url, headers=headers)
    return req.text

def getCurse(firstDate, secondDate, valutId):
    url = 'http://cbr.ru/currency_base/dynamics.aspx?VAL_NM_RQ=%s&date_req1=%s&date_req2=%s' % (valutId, firstDate, secondDate)
    data = []
    code = getPage(url)
    bs = BeautifulSoup(code, "lxml")
    table = bs.find('table', {'class':'data'})
    rows = table.findAll('tr')
    for row in rows:
        col = row.findAll('td')
        col = [el.text.strip() for el in col]
        data.append([el for el in col if el])    

    data = data[1:]
    return data
    
if __name__ == "__main__":
    dollarId = 'R01235'
    euroId = 'R01239'
    ienaId = 'R01820'
    firstDate = '10.01.2014'
    secondDate = '19.03.2018'
    
    db.dollarRate.drop()
    db.euroRate.drop()
    db.ienaRate.drop()
        
    while True:
        print('1. Курс валют за период')
        print('2. Максимум курса за период')
        print('3. Минимум курса за период')
        print('4. Выход')
        par = int(input())
        if par == 4:
            exit()
        else:
            period = input('Период в формате DD.MM.YYYY-DD.MM.YYYY: ')
            borders = period.split('-')
            start = datetime.strptime(borders[0], '%d.%m.%Y')
            end = datetime.strptime(borders[1], '%d.%m.%Y')
            print('1. DOL/RUB')
            print('2. EUR/RUB')
            print('3. IENA/DOL')
            parFirst = int(input())
            if par == 1:
                graph = []
                dates = []
                if parFirst == 1:
                    result = dollarRate.find({"date": {'$lt': end, '$gte': start}})
                elif parFirst == 2:
                    result = euroRate.find({"date": {'$lt': end, '$gte': start}})
                elif parFirst == 3:
                    result = ienaRate.find({"date": {'$lt': end, '$gte': start}})
                for rt in result:
                    print(rt['date'], rt['value'])
                    dates.append(rt['date'])
                    graph.append(rt['value'])
                plt.plot_date(dates, graph, fmt='b-')
                #plt.plot(graph)
                plt.show()
            elif par == 2:
                if parFirst == 1:
                    result = dollarRate.find_one({"date": {'$lt': end, '$gte': start}, 'value': {'$exists':True}}, sort=[('value', -1)])
                elif parFirst == 2:
                    result = euroRate.find_one({"date": {'$lt': end, '$gte': start}, 'value': {'$exists':True}}, sort=[('value', -1)])
                elif parFirst == 3:
                    result = ienaRate.find_one({"date": {'$lt': end, '$gte': start}, 'value': {'$exists':True}}, sort=[('value', -1)])
                print('maximum = ', result['date'], result['value'])
            elif par == 3:
                if parFirst == 1:
                    result = dollarRate.find_one({"date": {'$lt': end, '$gte': start}, 'value': {'$exists':True}}, sort=[('value', 1)])
                elif parFirst == 2:
                    result = euroRate.find_one({"date": {'$lt': end, '$gte': start}, 'value': {'$exists':True}}, sort=[('value', 1)])
                elif parFirst == 3:
                    result = ienaRate.find_one({"date": {'$lt': end, '$gte': start}, 'value': {'$exists':True}}, sort=[('value', 1)])
                print('minimum = ', result['date'], result['value'])
